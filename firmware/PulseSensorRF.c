#define BYTE unsigned char
#define WORD unsigned int
#define DWORD unsigned long
#define OutDataSize 10
// izmenil tsv #define StopByte 0x02
// izmenil tsv #define StartByte 0x01
#define SampleStep 100       // *0.1ms
#define Timer1_200Hz 63000   //  200 Hz
#define FILTER_LENGTH 10
#define PROTOCOL_ASCII
// Protocol definitions
#define StartMeasuring 0x01
#define StopMeasuring 0x02
// izmenil tsv #define SetCode        0x03
#define DACOUTPUT 127          //1.5V
#define BTN_ON_THRESHOLD 50
#define BTN_OFF_THRESHOLD 20
#define BTN_IO              RA3_bit
#define SLEEP_IO            LATA4_bit
#define LED                 LATC3_bit
#define SLEEP_TRIS

// dobavil tsv
   #define StartByte 0x3A
   #define StopByte  0x0D
   #define IdentifyDevice 0x03
   #define StartMeasureAcceleration 0x05
   #define StartMeasureGyro 0x06
   #define SetRange 0x07
   // sensor to pc codes
   #define MainData 0x01
   #define IdentifyDevice 0x03
   #define AccelerationData 0x05
   #define GyroData 0x06
   #define PulseData 0x07
   #define TPulseWaveSensor 0x03
//
BYTE InBuff[16];
BYTE InData[16];
BYTE i,j, StartDetected, InDataReady,OutDataReady, Counter,WasStarted, SampleStepCounter, OutCounter;
DWORD TimeTick; // 0.1ms counter
BYTE Unpressed_Count, Pressed_Count;
  DWORD a;
WORD  OutDataBuff[OutDataSize];
WORD filter_buff[FILTER_LENGTH];
BYTE filter_ndx;
DWORD OutAsciiBuff[OutDataSize];
DWORD AverageBuff=0;
WORD Val;
WORD Average=0;
WORD AverageCounter=0;
#define AV_SAMPLES_COUNT 1024
WORD cnt,ledTick;
BYTE uart_rd;

BYTE Dummy;
BYTE AsciiToByte[71] = {
0,0,0,0,0,0,0,0,0,0,//9
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0, //47
0,1,2,3,4,5,6,7,8,9, //57
0,0,0,0,0,0,0,//64
10,11,12,13,14,15};
BYTE ByteToAscii[16] = {0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x41,0x42,0x43,0x44,0x45,0x46};
bit oldstate;
bit SLEEP_WP;
bit Bluetooth_activ;
//WORD t=0;
void InitMain (){
 // oscillator
     //OSCCON = 0x6A; // 4MHz, INTOSC
     OSCCON = 0x7A;   // 16MHz, INTOSC
     //port io
     TRISA = 0x0B; // 1011
     TRISC3_bit = 0;               // RC3 is output  in/off LED
     TRISC0_bit = 1;
     TRISC1_bit = 1;
     TRISC2_bit = 0;

     RC4PPS = 0x14;// TX pin on RC4
     RXPPS =  0x15;// RX pin on RC5
     // ADC
     ANSELA = 0x03;
     ADCON0 = 0x00; // select  AN0
     ADCON1 = 0x02;// FOSC/64
     ADCON2 = 0x00;
     ADFM_bit=1;
     //ADC_Init();
     //C1ON_bit = 0;               // Disable comparators
     //C2ON_bit = 0;

     // Amplifier
      ANSELC = 0x07;
      OPA1CON = 0x00;   // op amp disabled, inv input connected to OPA0IN,
                       // non inv input connected to OPA1IN+ pin
     // DAC
     DAC1CON0 = 0x14; // DAC off, DAC out 2 pin, VREF on VREF+
     // Timer 0 config
     PSA_bit=0;
     PS0_bit = 0; // 1MHZ timer clock
     PS1_bit = 0; // 1MHZ timer clock
     PS2_bit = 0; // 1MHZ timer clock
     TMR0CS_bit=0;
     TMR0IE_bit=1;
     GIE_bit = 1;
     // Timer 1 cfg 10 ms
     TMR1CS0_bit = 0; // Fosc/4
     TMR1CS1_bit = 0; // Fosc/4
     T1CKPS0_bit = 1; // 1:8 prescaler
     T1CKPS1_bit = 1; // 1:8 prescaler
     PEIE_bit=1;
     TMR1IE_bit=1;
     // USART
     TX1STA = 0x00;
     BRGH_bit = 1;
     TXEN_bit=1;
     RC1STA=0x00;
     SREN_bit=1;
     SPEN_bit=1;
// izmenil tsv
     SP1BRGL = 8;     //config 115200 at fosc=16MHz
     //SP1BRGL = 105;    //9600 bs   at fosc=16MHz
     UART1_Init(115200);
     //UART1_Init(38400);
     LED=1;
//
     //LATA4_bit =1; // +3.3V on     off sleep mode
     SLEEP_IO=1;
    // WPUA3_bit=1;
     LATC3_bit=0;
     TRISA = 0x0B;
     oldstate=0;

     IOCAP3_bit=1;
     IOCIE_bit=1;
    // UART1_Write_Text("AT+UART=115200,0,0\r\n");

}
void interrupt ()        //
{
    if (IOCAF3_bit)
    {
     IOCAF3_bit=0;
     if (SLEEP_IO==1){SLEEP_IO=0; LED=0;
                               TMR1ON_bit=0;         // ????. Timer
                              //ADON_bit=0;           // ????. ???
                              //DACEN_bit=0;          // ????. ???
                              //OPA1EN_bit=0;         // ????. ??
 // izmenil tsv - dobavil asm reset;
     asm sleep;asm reset;} else {SLEEP_IO=1;  LED=1;TimeTick=0; Bluetooth_activ=0; }           // TimeTick=0;  SLEEP_WP=1;

    }

    if (TMR0IF_bit)
    {
        //if (TimeTick%5000==0)  LED=~LED;   //  if (SLEEP_IO==1)
        TMR0=50;
        if (ledTick>5000)
        {
                 LED=~LED;
                 ledTick=0;
        }
        TMR0IF_bit=0;
        TimeTick++;
        ledTick++;
    }
    if (TMR1IF_bit)
    {
        TMR1IF_bit = 0;
        TMR1 = Timer1_200Hz;
        if (OutDataReady) return;
        ADGO_bit=1;
        while(ADGO_bit);
        //Val = ADC_Get_Sample(0);
        filter_buff[filter_ndx] = ADC_Get_Sample(0);
        filter_ndx++;
        if (filter_ndx >= FILTER_LENGTH) filter_ndx=0;
        Val = 0;
        for(i=0;i<FILTER_LENGTH;i++) Val += filter_buff[i];
        OutDataBuff[OutCounter] = Val/FILTER_LENGTH;
        OutAsciiBuff[OutCounter]=0x00000000;
        OutAsciiBuff[OutCounter]|=(DWORD)ByteToAscii[(OutDataBuff[OutCounter]>>12)&0x000F]<<24;
        OutAsciiBuff[OutCounter]|=(DWORD)ByteToAscii[(OutDataBuff[OutCounter]>>8)&0x000F]<<16;
        OutAsciiBuff[OutCounter]|=(DWORD)ByteToAscii[(OutDataBuff[OutCounter]>>4)&0x000F]<<8;
        OutAsciiBuff[OutCounter]|=(DWORD)ByteToAscii[OutDataBuff[OutCounter]&0x000F];
        OutCounter++;
        if (OutCounter==OutDataSize)
        {
            OutCounter=0;
            OutDataReady=1;
        }
    }
}

void RecieveData()
{
    if (UART1_Data_Ready())
            if (StartDetected)
            {
                InBuff[Counter] = UART1_Read();
                if (InBuff[Counter]==StopByte)
                {
                    for(i=0;i<Counter/2;i++){
                        Dummy = AsciiToByte[InBuff[i*2+1]];
                        Dummy |= AsciiToByte[InBuff[i*2]]<<4;
                        InData[i] = Dummy;
                    }
                    StartDetected=0;
                    InDataReady=1;
                }
                Counter++;
            } else{

                Dummy = UART1_Read();
                if(Dummy==StartByte)
                {
                    StartDetected=1;
                    Counter=0;
                }
            }
}
void SendData()
{
    UART1_Write(StartByte);
// izmenil tsv - dobavil kod paketa 0x07;
    UART1_Write(ByteToAscii[PulseData >> 4]);
    UART1_Write(ByteToAscii[PulseData & 0x0F]);
//
    for(i=0;i<OutDataSize;i++)
    {
        UART1_Write(OutAsciiBuff[i]>>24);
        UART1_Write(OutAsciiBuff[i]>>16);
        UART1_Write(OutAsciiBuff[i]>>8);
        UART1_Write(OutAsciiBuff[i]);
    }
    UART1_Write(StopByte);
}

void main() {
  InitMain();
  StartDetected=0; Dummy=0; Counter=0;  InDataReady=0; OutCounter=0;filter_ndx=0;
  TimeTick=0;   SLEEP_WP=0;  Bluetooth_activ=0;
  /*
  TMR1ON_bit=1;         // ???. Timer
  ADON_bit=1;           // ???. ???
  DACEN_bit=1;          // ???. ???
  DAC1CON1 = DACOUTPUT; // 1.5 V ???
  OPA1EN_bit=1;         // ???. ??
    */
  while(1)
    {
          //if (TimeTick>4500000) if (Bluetooth_activ==0)  {SLEEP_IO=0; LED=0; asm sleep;} // ?????????? ???? ?????????
          RecieveData();
          if (InDataReady)
          {  switch(InData[0])
              {
                  // izmenil tsv
                  //case SetCode:
                  //      UART1_Write(StartByte);
                  //      UART1_Write(0x30);
                  //      UART1_Write(0x33);
                  //      UART1_Write(0x30);
                  //      UART1_Write(0x31);
                  //      UART1_Write(StopByte);
                  //      break;
                  case StartMeasuring:
                              Bluetooth_activ=1;
                              TMR1ON_bit=1;         // ???. Timer
                              ADON_bit=1;           // ???. ???
                              DACEN_bit=1;          // ???. ???
                              DAC1CON1 = DACOUTPUT; // 1.5 V ???
                              OPA1EN_bit=1;         // ???. ??
                              //WasStarted=1;
                              break;
                  case StopMeasuring:
                              //WasStarted=0;
                              TMR1ON_bit=0;         // ????. Timer
                              ADON_bit=0;           // ????. ???
                              DACEN_bit=0;          // ????. ???
                              OPA1EN_bit=0;         // ????. ??
                              break;
//  izmenil tsv
                  case IdentifyDevice:
                      UART1_Write(StartByte);
                      UART1_Write(ByteToAscii[IdentifyDevice >> 4]);
                      UART1_Write(ByteToAscii[IdentifyDevice & 0x0F]);
                      UART1_Write(ByteToAscii[TPulseWaveSensor >> 4]);
                      UART1_Write(ByteToAscii[TPulseWaveSensor & 0x0F]);
                      UART1_Write(StopByte);
                      break;
//
                  default: break;
              }
              InDataReady=0;
          }
          /*
          if (AverageCounter>=AV_SAMPLES_COUNT)
          {
             Average = AverageBuff >> 10;
             AverageBuff=0;
             AverageCounter=0;
          }
          */
          if (OutDataReady)
          {
              OutDataReady=0;
              SendData();
              LED=~LED;
          }
    }

}